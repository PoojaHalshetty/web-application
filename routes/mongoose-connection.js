var express = require('express');
var app = module.exports = express();
var bodyParser = require('body-parser');
var session = require('express-session');
app.use(bodyParser.json());
var errorHandler = require('errorhandler');
app.use(bodyParser.urlencoded({ extended: false }))
var urlencodedParser = bodyParser.urlencoded({ extended: false })
const { body, validationResult } = require('express-validator');
var Connections = require('../models/mongo-ConnectionDB');
app.use(bodyParser.json());
var url = "mongodb://localhost/LetsMeet";
var userProfileDB = require('../models/UserProfileDB');
var newConnectionCount = 10;
/* mongoose */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./../models/mongo-UserDB');
var url = "mongodb://localhost/LetsMeet";


var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use('/assets', express.static('assets'));

app.use(session({
  secret: 'iloveviv',
  resave: true,
  saveUninitialized: true
}));


module.exports = function (app) {

  app.get('/connections', function (req, res) {
    if (req.session.theUser === undefined) {
      var user = false;
    }
    else {
      var user = true;
    }
    mongoose.connect(url, function (err, db) {
      if (err) throw err;
      db.collection('Connections').find().toArray((err, allConnections) => {
        if (err) return console.log(err);
        console.log('allConnections', allConnections);
        res.render('connections', { user: user, connection: allConnections });
      });
    });
  });

  /* Connection page */

  app.get('/connection', function (req, res) {
    if (req.session.theUser === undefined) {
      var user = false;
    }
    else {
      var user = true;
    }
    var searchConID = req.query.connectionID;
    mongoose.connect(url, function (err, db) {
      if (err) throw err;
      console.log('searchConID', searchConID);
      db.collection('Connections').find({ connectionID: searchConID }).toArray((err, oneConnection) => {
        if (err) return console.log(err);
        console.log('OneConnection in /connection', oneConnection);
        res.render('connection', { user: user, connection: oneConnection });
      });
    });
  });


  /*connections page */
  app.get('/connection?connectionID*', function (req, res) {
    if (req.session.theUser === undefined) {
      var user = false;
    }
    else {
      var user = true;
    }
    var searchConID = req.query.connectionID;
    mongoose.connect(url, function (err, db) {
      if (err) throw err;
      db.collection('Connections').find({ connectionID: searchConID }).toArray((err, oneConnection) => {
        if (err) return console.log(err);
        console.log('OneConnection in connection?connectionID*', oneConnection);
        res.render('connection', { user: user, connection: oneConnection });
      });
    });
  });

  app.get('/newConnection', function (req, res) {
    console.log('Get connection req.body', req.body);
    if (req.session.theUser === undefined) {
      if (req.session.theUser === undefined) {
        var user = false;
      }
      else {
        var user = true;
      }
      var userPresent = true;
      res.render('login', { user: user, userPresent: userPresent });
    }
    else {
      if (req.session.theUser === undefined) {
        var user = false;
      }
      else {
        var user = true;
      }
      res.render('newConnection', { user: user });
    }
  });

  app.post('/newConnection', urlencodedParser, function (req, res) {

    const errors = validationResult(req);
    body('connectionName', 'connectionName must not be empty.').trim(),
      body('connectionTopic', 'connectionTopic must not be empty ').trim()

    if (!errors.isEmpty()) {
      req.flash('error', "Invalid input");
      res.redirect('/newConnection');
    }
    else {
      if (req.session.theUser !== undefined) {
        console.log('Post connection req.query.connectionName', req.body.connectionName);

        var user = true;

        var newuID = req.session.theUser;
        newCreatedUserID = newuID[0].userID;

        function getRandomIntInclusive(min, max) {
          min = Math.ceil(min);
          max = Math.floor(max);
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        newConnectionCount = getRandomIntInclusive(20, 1000) + 1;
        //newConnectionID = 'con' + getRandomIntInclusive(40, 100);
        newConnectionID = 'con' + newConnectionCount++;
        console.log('newConnectionID', newConnectionID);


        userProfileDB.saveNewConnection(newConnectionID, req.body.connectionName, req.body.connectionTopic, req.body.description1, req.body.description2, req.body.date, req.body.time, newCreatedUserID);

        Connections.getConnections()

        mongoose.connect(url, function (err, db) {
          if (err) throw err;
          db.collection('Connections').find().toArray((err, allConnections) => {
            if (err) return console.log(err);
            console.log('allConnections after adding New Connection', allConnections);
            res.render('connections', { user: user, connection: allConnections });
          });
        });

      }
      else {
        var user = false;
        var userPresent = true;
        console.log('User is not logged IN in /post new connection');
        res.render('login', { user: user, userPresent: userPresent });

      }
    }
  });


}

