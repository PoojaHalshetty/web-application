var express = require('express');
var app = module.exports = express();
var bodyParser = require('body-parser');
var session = require('express-session');
app.use(bodyParser.json());
var errorHandler = require('errorhandler');
app.use(bodyParser.urlencoded({ extended: false }))
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var Connections = require('../models/mongo-ConnectionDB');
app.use(bodyParser.json());
var url = "mongodb://localhost/LetsMeet";
var userProfileDB = require('../models/UserProfileDB');
const { body, validationResult } = require('express-validator');
app.use(express.json());
app.use(bodyParser.text());
/* mongoose */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Connection = require('../models/mongo-ConnectionDB');
var User = require('../models/mongo-UserDB');
var url = "mongodb://localhost/LetsMeet";
var alert = require('alert-node')

'use strict';
var crypto = require('crypto');

var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use('/assets', express.static('assets'));

app.use(session({
    secret: 'iloveviv',
    resave: true,
    saveUninitialized: true
}));


module.exports = function (app) {


    app.get('/login', function (req, res) {
        var user;
        if (req.session.theUser === undefined) {
            user = false;
        }
        else {
            user = true;
        }

        var userPresent = true;
        SessionUser = req.session.theUser;
        console.log('inside get/login --- req body', req.body);
        res.render('login', { user: user, userPresent: userPresent });
    });


    app.post('/login', urlencodedParser, function (req, res) {

        const errors = validationResult(req);
        body('emailID', 'EmailID must not be empty.').trim(),
            body('password', 'Password must not be empty and min 8 characters.').trim().isLength({ min: 8 });
        var email = req.body.email;
        var password = req.body.password;
        if (!errors.isEmpty()) {
            req.flash('error', "Either username or password are incorrect. Please try again.");
            res.redirect('/login');
        }
        else {
            mongoose.connect(url, function (err, db) {

                if (err) throw err;

                db.collection('Users').find({ email: email, password: password }).toArray((err, loginUser) => {
                    if (err) throw err;

                    console.log('Login user details ', loginUser);
                    if (loginUser[0] === undefined) {

                        console.log('User not in DB');
                        var userPresent = false;

                        if (req.session.theUser === undefined) {
                            var user = false;
                        }
                        else {
                            var user = true;
                        }
                        res.render('login', { user: user, userPresent: userPresent });

                    }
                    else {

                        console.log('Login user ID ', loginUser[0].userID);
                        loginUserID = loginUser[0].userID;

                        db.collection('UserConnections').aggregate([
                            {
                                $match: {
                                    userID: loginUserID
                                }
                            },

                            {
                                $lookup:

                                {

                                    from: 'Connections',

                                    localField: 'connectionID',

                                    foreignField: 'connectionID',

                                    as: 'connection'

                                }

                            }

                        ]).toArray(function (err, allConnections) {
                            console.log('allconnections below');
                            console.log(JSON.stringify(allConnections));
                            if (err) throw err;

                            db.collection('Users').find({ userID: loginUserID }).toArray((err, userDetails) => {

                                if (err) return console.log(err);

                                console.log('userDetails', userDetails);

                                console.log(JSON.stringify(userDetails));
                                req.session.theUser = userDetails;
                                console.log('Inside app.post else part userDetails = req.session.theUser', req.session.theUser);
                                var user;
                                if (req.session.theUser === undefined) {
                                    user = false;
                                }
                                else {
                                    user = true;
                                }
                                res.render('savedConnections', { user: user, userConnections: allConnections, userDetails: userDetails });
                            });
                        });


                    }
                });

            });
        }

    });

    /* Sign-up*/
    app.get('/sign-up', function (req, res) {
        console.log('inside get/signup');
        if (req.session.theUser === undefined) {
            var user = false;
        }
        else {
            var user = true;
        }
        var userPresent = false;
        res.render('sign-up', { user: user, userPresent: userPresent });
    });


    app.post('/sign-up', urlencodedParser, function (req, res) {

        const errors = validationResult(req);
        body('emailID', 'EmailID must not be empty.').trim(),
            body('password', 'Password must not be empty and min 8 characters.').trim().isLength({ min: 8 });
        body('firstName', 'firstName must not be empty.').trim(),
            body('lastName', 'lastName must not be empty.').trim();
        var email = req.body.email;
        var password = req.body.password;
        var firstName = req.body.firstName;
        var lastName = req.body.lastName;

        if (!errors.isEmpty()) {
            req.flash('error', "Either username or password are incorrect. Please try again.");
            res.redirect('/login');
        }
        else {
            mongoose.connect(url, function (err, db) {

                if (err) throw err;

                db.collection('Users').find({ email: email }).toArray((err, loginUser) => {
                    if (err) throw err;

                    console.log('Login user details ', loginUser);
                    if (loginUser[0] !== undefined) {
                        console.log('Login user ID ', loginUser[0].userID);
                        loginUserID = loginUser[0].userID;


                        console.log('User already in the DB');
                        var userPresent = true;

                        if (req.session.theUser === undefined) {
                            var user = false;
                        }
                        else {
                            var user = true;
                        }
                        res.render('sign-up', { user: user, userPresent: userPresent });

                    }
                    else {

                        // insert data
                        console.log("Ready to insert new user into the User Collection");
                        mongoose.connect(url, function (err, db) {
                            if (err) throw err;

                            /** Salting */
                            var genRandomString = function (length) {
                                return crypto.randomBytes(Math.ceil(length / 2))
                                    .toString('hex')
                                    .slice(0, length);
                            };

                            var sha512 = function (password, salt) {
                                var hash = crypto.createHmac('sha512', salt); /** sha512 hashing algorithm*/
                                hash.update(password);
                                var value = hash.digest('hex');
                                return {
                                    salt: salt,
                                    passwordHash: value
                                };
                            };
                            function saltHashPassword(userpassword) {
                                var salt = genRandomString(16); /** salt of length 16 */
                                var passwordData = sha512(userpassword, salt);
                                console.log('UserPassword = ' + userpassword);
                                console.log('Passwordhash = ' + passwordData.passwordHash);
                                console.log('nSalt = ' + passwordData.salt);
                                return passwordData.salt;
                            }

                            var hashedpassword = saltHashPassword(password);
                            console.log('hashedpassword', hashedpassword);

                            db.collection('Users').find({}).count().then(function (result) {
                                console.log('userID', result);
                                var userID = result + 1;
                                db.collection('Users').insertOne({ userID: userID, firstName: firstName, lastName: lastName, email: email, password: password, hashedpassword: hashedpassword });
                                return;
                            });

                            console.log('New User Saved');
                            alert('New User Saved');
                            var user;
                            if (req.session.theUser === undefined) {
                                user = false;
                            }
                            else {
                                user = true;
                            }
                            res.redirect('login');

                        });

                    }
                });

            });
        }

    });

}

