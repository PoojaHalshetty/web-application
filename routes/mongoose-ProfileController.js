var express = require('express');
var app = module.exports = express();
var bodyParser = require('body-parser');
var session = require('express-session');
app.use(bodyParser.json());


/* mongoose */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Connection = require('../models/mongo-ConnectionDB');
var User = require('./../models/mongo-UserDB');
var url = "mongodb://localhost/LetsMeet";


var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use('/assets', express.static('assets'));

app.use(session({
  secret: 'iloveviv',
  resave: true,
  saveUninitialized: true
}));


module.exports = function (app) {

  app.get('/savedConnections', function (req, res) {
    if (req.session.theUser === undefined) {
      console.log('Inside /saved Connections');
      var user = "false";
      res.render('unregistered-user', { user: user });
    }
    else {
      if (req.query.action === 'update') {
        if (req.query.RSVP === undefined) {
          if (req.session.theUser === undefined) {
            var user = false;
          }
          else {
            var user = true;
          }
          var searchConID = req.query.theConnection;
          mongoose.connect(url, function (err, db) {
            if (err) throw err;
            console.log('searchConID', searchConID);
            db.collection('Connections').find({ connectionID: searchConID }).toArray((err, oneConnection) => {
              if (err) return console.log(err);
              console.log('OneConnection in /savedConnections', oneConnection);
              res.render('connection', { user: user, connection: oneConnection });
            });
          });

        }
        // http://127.0.0.1:8084/savedConnections?action=update&RSVP=Yes&theConnection=con1
        if (req.query.RSVP !== undefined) {
          console.log('in req.session.RSVP!==undefined');
          var userID = req.session.theUser;
          console.log('useriddddddddddd', userID[0].userID);
          console.log('req.sesssion.theUser', req.session.theUser);
          mongoose.connect(url, function (err, db) {
            if (err) throw err;
            db.collection('UserConnections').updateOne(
              { "connectionID": req.query.theConnection },
              { $set: { "RSVP": req.query.RSVP, "userID": userID[0].userID } },
              { upsert: true }
            );

            console.log('RSVP updated');

            db.collection('UserConnections').aggregate([
              {
                $match: {
                  userID: userID[0].userID
                }
              },


              {
                $lookup:

                {

                  from: 'Connections',

                  localField: 'connectionID',

                  foreignField: 'connectionID',

                  as: 'connection'

                }

              }

            ]).toArray(function (err, allConnections) {
              console.log(JSON.stringify(allConnections));
              if (err) throw err;

              db.collection('UserConnections').find({}).toArray((err, userDetails) => {

                if (err) return console.log(err);

                console.log('userDetails', userDetails);

                console.log(JSON.stringify(userDetails));
                // req.session.theUser = userDetails;
                console.log('Inside  if (req.query.RSVP !== undefined) userDetails = req.session.theUser', req.session.theUser);
                var user;
                if (req.session.theUser === undefined) {
                  user = false;
                }
                else {
                  user = true;
                }
                res.render('savedConnections', { user: user, userConnections: allConnections, userDetails: userDetails });
              });
            });

          });
        }
      }
      if (req.query.action === 'delete') {

        console.log('Inside req.action.delete');
        mongoose.connect(url, function (err, db) {
          if (err) throw err;
          var userID = req.session.theUser;
          console.log('useriddddddddddd in delete', userID[0].userID);

          db.collection('UserConnections').deleteOne({ "connectionID": req.query.theConnection });

          console.log('Connection deleted');

          db.collection('UserConnections').aggregate([
            {
              $match: {
                userID: userID[0].userID
              }
            },


            {
              $lookup:

              {

                from: 'Connections',

                localField: 'connectionID',

                foreignField: 'connectionID',

                as: 'connection'

              }

            }

          ]).toArray(function (err, allConnections) {
            console.log(JSON.stringify(allConnections));
            if (err) throw err;

            db.collection('UserConnections').find({}).toArray((err, userDetails) => {

              if (err) return console.log(err);

              console.log(JSON.stringify(userDetails));
              // req.session.theUser = userDetails;

              userDetails = req.session.theUser;
              var user;
              if (req.session.theUser === undefined) {
                user = false;
              }
              else {
                user = true;
              }
              res.render('savedConnections', { user: user, userConnections: allConnections, userDetails: userDetails });
            });
          });

        });



      }
      if (req.query.action !== 'delete' && req.query.action !== 'update') {
        res.render('inValid', { user: user });

      }
    }
  });

  app.get('/savedConnections/clearSession', function (req, res, next) {

    var SessionUser = req.session.theUser;
    var SessionProfile = req.session.currentProfile;
    console.log('SessionUser in clear session ', SessionUser);
    console.log('SessionProfile in clear session ', SessionProfile);
    console.log('req.session deletion ' + req.session);


    // let userProfile = User.deleteUserProfile();
    // SessionProfile = userProfile;
    req.session = undefined;
    var user = false;
    console.log('session data after delete ' + req.session);
    res.render('index', { user: user });
  });
}

