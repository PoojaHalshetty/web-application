var express = require('express');
var app = express();
const path = require('path');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var session = require('express-session');
var profile_controller = require('./routes/mongoose-ProfileController.js');
var connection_controller = require('./routes/mongoose-connection.js');
var login_controller = require('./routes/mongoose-loginController.js');
var cookieSession = require('cookie-session');
var mongoose = require('mongoose');
app.use(bodyParser.json());
var url = "mongodb://localhost/LetsMeet";
app.use(bodyParser.urlencoded({ extended: false }))
app.set('trust proxy', 1);
/* cookie session */
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2']
}));
/*
var flash = require('connect-flash');
app.use(flash()); // flash messages

app.use(function (req, res, next) {
    res.locals.success = req.flash('success');
    res.locals.info = req.flash('info');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
  });

*/
app.set('view engine', 'ejs');

/* Set path for views and assets */
app.set('views', path.join(__dirname, './views'));
app.use('/assets', express.static(path.join(__dirname, './assets')));



/* Mongo DB Conenctions */
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('We are connected to LetsMeet DB');
});
var MongoStore = require('connect-mongo')(session);

/** register Session */
app.use(session({
  secret: 'iloveviv',
  resave: true,
  saveUninitialized: true,
  store: new MongoStore({
    mongooseConnection: db
  })
}));


app.use(function printSession(req, res, next) {
  console.log('req.session.theUser', req.session.theUser);
  return next();
});

var SessionUser;

/*Login */
require('./routes/mongoose-loginController')(app);
app.use('/login', login_controller);
app.use('/sign-up', login_controller);

/* home page */
app.get('/', function (req, res) {
  /** If user is logged in, display index page with user name */
  SessionUser = req.session.theUser;

  if (req.session.theUser === undefined) {
    var user = false;
  }
  else {
    var user = true;
  }
  console.log('Invoking home page');
  console.log('SessionUser', SessionUser);
  console.log("Current User: " + user);
  res.render('index', { user: user });
});

app.get('/index', function (req, res) {
  console.log('Invoking home page');
  var SessionUser = req.session.theUser;
  if (SessionUser === undefined) {
    var user = false;
  }
  else {
    var user = true;
  }
  console.log("Current User: " + user);
  res.render('index', { user: user });
})

/*connections page */

require('./routes/mongoose-connection.js')(app);
app.use('/connections', connection_controller);
app.use('/connection', connection_controller);


/* about page */
app.get('/about', function (req, res) {
  console.log('Invoking about page');
  if (req.session.theUser === undefined) {
    var user = false;
  }
  else {
    var user = true;
  }
  console.log("Current User: " + user);
  res.render('about', { user: user });
})

/* contact page */
app.get('/contact', function (req, res) {
  console.log('Invoking contact page');
  if (req.session.theUser === undefined) {
    var user = false;
  }
  else {
    var user = true;
  }
  console.log("Current User: " + user);
  res.render('contact', { user: user });
})

/* savedConnections page */
require('./routes/mongoose-ProfileController.js')(app);
app.use('/savedConnections', profile_controller);

console.log('in app.js before going inside connection');
/*clear session */

app.use('/savedConnections/clearSession', profile_controller);
/* newConnection page*/

require('./routes/mongoose-connection.js')(app);
app.use('/newConnection', connection_controller);


// listening server 
app.listen(8084, function (err) {
  console.log('Application started')
  console.log('listening on port 8084')
  if (err) {
    console.log("error while starting server");
  }
  else {
    console.log("server has been started at port " + 8084);
  }
})







