var mongoose = require('mongoose');
var Connections = require('./mongo-ConnectionDB');
var Users = require('./../models/mongo-UserDB');
var UserConnections = require('./../models/mongo-UserConnectionDB');
var url = "mongodb://localhost/LetsMeet";

/** 
 * 1. getUserProfile(email/userID) - this function returns a collection/list of all UserConnection objects saved
 */

module.exports.getUserProfile = function (email) {

    Users.find({ emailAddress: email }, function (err, userConnectionDetails) {
        if (err) throw err;

        // object of the user
        console.log(userConnectionDetails);
        return userConnectionDetails;
    });
}

/**
 * 2. addRSVP(connectionID, userID, rsvp) –  this function associates a connection to the user with userID (when the user saves the connection) and updates the RSVP that user provides for a connection with this connection ID. The corresponding database document can use the userID and connectionID as unique identifiers (keys). 
 */

module.exports.addRSVP = function (connectionID, userID, RSVP) {
    UserConnections.findOneAndUpdate({ userID: userID, connectionID: connectionID }, { $set: { connectionID: connectionID, userID: userID, RSVP: RSVP } }, { upsert: true }, function (err, newConnection) {
        if (err) return res.send(500, { error: err });
        return newConnection;
    });
}

module.exports.saveNewConnection = function (connectionID, connectionName, connectionTopic, description1, description2, date, time, Connection_Created_UserID) {

    mongoose.connect(url, function (err, db) {
        if (err) throw err;
        db.collection('Connections').insertOne({ connectionID: connectionID, connectionName: connectionName, connectionTopic: connectionTopic, description1: description1, description2: description2, date: date, time: time, Connection_Created_UserID: Connection_Created_UserID });
        console.log('New Connection Saved');

    });

}


