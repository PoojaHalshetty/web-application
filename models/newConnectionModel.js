/* Javascript to descibe NewConnection model having topic, name, details, where, when */

var newConnection = function(connectionTopic, connectionName, description1, description2, date, time){
    var newConnectionModel = {connectionTopic:connectionTopic, connectionName:connectionName, description1:description1, description2:description2, date:date, time:time};
    return newConnectionModel;
    };
    
module.exports.newConnection = newConnection;
