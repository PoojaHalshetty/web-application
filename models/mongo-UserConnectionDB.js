var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/LetsMeet', { useNewUrlParser: true });
var Connection = require('./mongo-ConnectionDB');
var User = require('./mongo-UserDB');
var Schema = mongoose.Schema;

var UserConnectionSchema = new mongoose.Schema({
    connectionID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Connection'
    },
    userID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    RSVP: String
});

var userConnection = mongoose.model('userConnection', UserConnectionSchema);

module.exports.getConnection = function (id) {
    var data = connection.find({ connectionID: id });
    return data;
}

