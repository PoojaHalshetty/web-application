var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/LetsMeet', { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('We are connected to LetsMeet DB');
});

var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var Schema = mongoose.Schema;


// Connection Schema
var UserSchema = new Schema({
    userID:
    {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
      },
    password: {
      type: String,
      required: true
    },

    firstName:
    {
        type: String,
        required: true
    },
    lastName:
    {
        type: String,
        required: true
    },
    address1Field : String,
    address2Field : String,
    city : String,
    state : String,
    country : String
});

var User = mongoose.model('Users', UserSchema);

UserSchema.plugin(autoIncrement.plugin, 'Users'); 

module.exports=function getUser(email){

    User.find({ email: email }, function(err, user) {
        if (err) throw err;
      
        // object of the user
        console.log(user);
        return user;
      });
}

module.exports = User;


