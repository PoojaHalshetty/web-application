var express = require('express');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/LetsMeet', { useNewUrlParser: true });
var Schema = mongoose.Schema;
var url = "mongodb://localhost/LetsMeet";

// Connection Schema
var connectionSchema = new Schema({
    connectionID: String,
    connectionName:
    {
        type: String,
        required: true
    },
    connectionTopic:
    {
        type: String,
        required: true
    },
    description1: String,
    description2: String,
    date: String,
    time: String,
    URL: String,
    Connection_Created_UserID:
    {
        type: String,
        required: true
    }

});

var collectionName = "Connections";
var Connections = mongoose.model(collectionName, connectionSchema);

//getConnections() - this function returns an array of Connection objects of all the connections in the connections table from the database
module.exports.getConnections = function () {
    mongoose.connect(url, function (err, db) {
        if (err) throw err;  
        db.collection('Connections').find().toArray((err, allConnections) => {
            if (err) return console.log(err);
            console.log('allConnections',allConnections);
            return allConnections;
        });
      });
}

//getConnection(connectionID) - this function returns a Connection object for the provided connection code
module.exports.getConnection = function (connectionID) {
    var oneConnection = Connections.find({ connectionID: connectionID });
    console.log('One Connection inside getConnection(connectionID)', oneConnection)
    return oneConnection;
}



module.exports.saveNewConnection = function (connectionID, connectionName, connectionTopic, description1, description2, date, time, Connection_Created_UserID) {

    var newConnection = Connections.insertOne({ connectionID: connectionID, connectionName: connectionName, connectionTopic: connectionTopic, description1: description1, description2: description2, date: date, time: time, Connection_Created_UserID: Connection_Created_UserID }).then(function(err, nn){
        if(err) throw err;
        console.log(nn);
    });
    console.log('New connection added is', newConnection);

    var allConnections = Connections.find();
    return allConnections;

}


module.exports.Connections = Connections;
