var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var LoginSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true
  }
});
var userLogin = mongoose.model('Logins', LoginSchema);
LoginSchema.statics.authenticate = function (email, password, callback) {
  userLogin.findOne({ email: email })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found');
        err.status = 401;
        return callback(err);
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          return callback(null, user);
        } else {
          return callback();
        }
      })
    });
}

LoginSchema.pre('save', function (next) {
  var username = this;
  bcrypt.hash(user.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }
    username.password = hash;
    next();
  })
});

module.exports = userLogin;